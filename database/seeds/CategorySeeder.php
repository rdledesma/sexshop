<?php

use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'name' => 'Juguetes y Accesorios'
        ]);
        DB::table('categories')->insert([
            'name' => 'Lencería'
        ]);
        DB::table('categories')->insert([
            'name' => 'Cosméticos y Geles'
        ]);
    }
}
