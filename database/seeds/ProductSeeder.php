<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
            'name' => 'Íntima: Gel Femenino',
            'price' => 629.16,
            'quantity' => 5,
            'description' => 'Gel que intensifica la sensación de placer en la mujer. Se trata de una experiencia sensorial única que combina Mentol, Jojoba y L-arginina estimulando la erección del clítoris y labios menores y asegurando el orgasmo de manera fácil e intensa.

            L-Arginina es un aminoácido que interviene en la primera fase de la respuesta erectiva del clítoris y por acción de la enzima óxido-sintetasa, se convierte en óxido nítrico (es un gas vasodilatador que producirá el comienzo del proceso de vasodilatación en los cuerpos cavernosos de las estructuras clitoridianas), lo que resulta en un aumento de la capacidad eréctil del órgano erótico por excelencia en las mujeres.

            Así, en mujeres con dificultad para excitarse, deseo sexual disminuido o anaorgasmia, el aminoácido en su uso tópico (gel) podría aumentar la excitación, turgencia y erección clitoridianas favoreciendo el orgasmo. Aunque también incrementa el goce en mujeres que no tienen ninguna disfunción y simplemente desean sentir y gozar más!!

            La mujer experimenta un aumento de la excitación y sensibilidad del clítoris llegando a orgasmos más rápidos e intensos, lo que realza la experiencia sexual individual y de la pareja.',

            'review' => 'Alcanzá tu mejor orgasmo ¡Disfrútalo!',
            'measurement' => '50 ml',
            'isOffer' => 0,
            'discountPorcent'=>0,
            'showFlayer'=>1,
            'showCarousel'=>1,
            'type_id'=>12,
            'mark_id'=>1

        ]);

        DB::table('products')->insert([
            'name' => 'Ultra Mujer',
            'price' => 1018.64,
            'quantity' => 5,
            'description' => 'Forma de uso sugerido: tomar 1 cápsula a la hora del día que prefiera. Puede tomar hasta 2 cápsulas al día si lo desea, y en caso de tomar 2 puede tomarlas juntas.
            Ultra Mujer no es un medicamento y está aprobado en argentina para su comercialización sin ninguna clase de receta médica.',
            'review' => 'Alcanzá tu mejor orgasmo ¡Disfrútalo!',
            'measurement' => 'Paquete de 4 cápsulas.',
            'isOffer' => 1,
            'discountPorcent'=>12,
            'showFlayer'=>1,
            'showCarousel'=>1,
            'type_id'=>12,
            'mark_id'=>2
        ]);

        DB::table('products')->insert([
            'name' => 'Pheromonas Mujer',
            'price' => 935.00,
            'quantity' => 5,
            'description' => 'Tus encantos femeninos serán resaltados por esta fórmula maravillosa!. Si el deseo sexual de su pareja está disminuido por el stress o el agotamiento, esta sustancia revertirá este fenómeno cuando sea percibida por él. Utilízalo solo, o con tu perfume habitual. Demostrando confianza...

            ¿Qué es la Feromona? Es un producto químico que el cuerpo humano produce para atraer a los demás, si bien no es conscientemente percibido. Actúa cambiando de manera cierta nuestra conducta sexual en niveles no perceptibles e inconscientes. A esta sustancia se la denomina pheromona, que en griego significa "transmisor de la excitación". Es una sustancia que potencia el aroma natural que existe en el ser humano, actuando positivamente sobre su conducta sexual.',
            'review' => 'Alcanzá tu mejor orgasmo ¡Disfrútalo!',
            'measurement' => '30 ml',
            'isOffer' => 0,
            'discountPorcent'=>0,
            'showFlayer'=>1,
            'showCarousel'=>1,
            'type_id'=>12,
            'mark_id'=>3
        ]);

        DB::table('products')->insert([
            'name' => 'Pheromonas Hombre',
            'price' => 935.00,
            'quantity' => 5,
            'description' => 'Pheromonas Hombre es un imán para las mujeres hermosas. Imagínate una poderosa fuerza de atracción indetectable, que te signifique la conquista de las mujeres que siempre soñaste. Cuando una mujer percibe las moléculas de esta sustancia esparcidas por el aire, se produce una reacción biológica. Esta reacción efectúa su labor en el punto de la seducción de sexo en su cerebro, con el resultado de una excitación sexual inconsciente

            ¿Qué es la Feromona? Es un producto químico que el cuerpo humano produce para atraer a los demás, si bien no es conscientemente percibido. Actúa cambiando de manera cierta nuestra conducta sexual en niveles no perceptibles e inconscientes. A esta sustancia se la denomina pheromona, que en griego significa "transmisor de la excitación". Es una sustancia que potencia el aroma natural que existe en el ser humano, actuando positivamente sobre su conducta sexual.',
            'review' => 'Alcanzá tu mejor orgasmo ¡Disfrútalo!',
            'measurement' => '30 ml',
            'isOffer' => 0,
            'discountPorcent'=>0,
            'showFlayer'=>1,
            'showCarousel'=>1,
            'type_id'=>12,
            'mark_id'=>3
        ]);

        DB::table('products')->insert([
            'name' => 'Perfume Extrapheromen',
            'price' => 1099.00,
            'quantity' => 5,
            'description' => 'Para Hombre! Tiene en su esencia pheromonas extras. Es un perfume fresco, condimentado, oriental. La combinación de pheromonas extra junto al cedro, flor de olivar, acentos aromáticos y anisados la convierten en una fragancia única y extremadamente sensual. Botella pequeña ideal para llevarlo en la cartera y aplicar las veces que sea necesario.',
            'review' => 'Alcanzá tu mejor orgasmo ¡Disfrútalo!',
            'measurement' => '30 ml',
            'isOffer' => 0,
            'discountPorcent'=>0,
            'showFlayer'=>1,
            'showCarousel'=>1,
            'type_id'=>12,
            'mark_id'=>1
        ]);

        DB::table('products')->insert([
            'name' => 'Perfume Extrapherowomen',
            'price' => 1099.00,
            'quantity' => 5,
            'description' => 'Para Mujer! Tiene en su esencia mayor cantidad de feromonas. Es mucho mas que una fragancia, es un imán irresistible. Su fragancia combina una mezcla exquisita de frutas, vainilla, patchoulli de Indon, sándalo de la India, benzoino y labdano. Botella pequeña ideal para llevarlo en la cartera y aplicar las veces que sea necesario.',
            'review' => 'Alcanzá tu mejor orgasmo ¡Disfrútalo!',
            'measurement' => '30 ml',
            'isOffer' => 0,
            'discountPorcent'=>0,
            'showFlayer'=>1,
            'showCarousel'=>1,
            'type_id'=>12,
            'mark_id'=>1
        ]);
        DB::table('products')->insert([
            'name' => 'Perfume Extrapherowomen',
            'price' => 1342.21,
            'quantity' => 5,
            'description' => 'El más vendido. Un clásico perfume femenino con pheromonas. Exquisita fragancia que desborda los sentidos. Con frescas y sexys notas cítricas y un corazón floral de jasmín, rosa y Praliné Frances. ¡Irresistible!',
            'review' => 'Alcanzá tu mejor orgasmo ¡Disfrútalo!',
            'measurement' => '100 ml',
            'isOffer' => 0,
            'discountPorcent'=>0,
            'showFlayer'=>1,
            'showCarousel'=>1,
            'type_id'=>12,
            'mark_id'=>4
        ]);
        DB::table('products')->insert([
            'name' => 'For Him',
            'price' => 1342.21,
            'quantity' => 5,
            'description' => 'Un perfume inspirado en un hombre distinguido, misterioso y sensual. Este perfume masculino con pheromonas ha sido especialmente pensado y elaborado para aumentar el poder de seducción.',
            'review' => 'Alcanzá tu mejor orgasmo ¡Disfrútalo!',
            'measurement' => '100 ml',
            'isOffer' => 0,
            'discountPorcent'=>0,
            'showFlayer'=>1,
            'showCarousel'=>1,
            'type_id'=>12,
            'mark_id'=>4
        ]);

        DB::table('products')->insert([
            'name' => 'Vibrations Erotic Parfum',
            'price' => 690.00,
            'quantity' => 5,
            'description' => 'Forma parte de un emocionante mundo de placer y tentación. Es una fragancia vibracional que recorre los cuerpos para explorarlos a pleno. Está dirigido a una mujer independiente, vital, segura de sí y extremadamente sensual y provocadora. Su esencia chic contiene granada, caqui, loto, champaca, orquita, violeta negra, ámbar líquido, caoba y almizcle.',
            'review' => 'Alcanzá tu mejor orgasmo ¡Disfrútalo!',
            'measurement' => '50 ml',
            'isOffer' => 0,
            'discountPorcent'=>0,
            'showFlayer'=>1,
            'showCarousel'=>1,
            'type_id'=>12,
            'mark_id'=>1
        ]);
    }
}
