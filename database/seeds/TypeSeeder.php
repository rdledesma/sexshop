<?php

use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
            'name' => 'Vibradores',
            'category_id' => '1'
        ]);
        DB::table('types')->insert([
            'name' => 'Anillos',
            'category_id' => '1'
        ]);
        DB::table('types')->insert([
            'name' => 'Arnéses y Prótesis',
            'category_id' => '1'
        ]);
        DB::table('types')->insert([
            'name' => 'Bolas Chinas',
            'category_id' => '1'
        ]);
        DB::table('types')->insert([
            'name' => 'Consoladores y Dildos',
            'category_id' => '1'
        ]);
        DB::table('types')->insert([
            'name' => 'Doble Penetración',
            'category_id' => '1'
        ]);
        DB::table('types')->insert([
            'name' => 'Disfraces',
            'category_id' => '2'
        ]);
        DB::table('types')->insert([
            'name' => 'Slips',
            'category_id' => '2'
        ]);
        DB::table('types')->insert([
            'name' => 'Tangas',
            'category_id' => '2'
        ]);
        DB::table('types')->insert([
            'name' => 'Lencería Fina',
            'category_id' => '2'
        ]);
        DB::table('types')->insert([
            'name' => 'Playboy',
            'category_id' => '2'
        ]);
        DB::table('types')->insert([
            'name' => 'Afrodisíaco',
            'category_id' => '3'
        ]);
        DB::table('types')->insert([
            'name' => 'Aceites y Escencias',
            'category_id' => '3'
        ]);
        DB::table('types')->insert([
            'name' => 'Geles y Lubricantes',
            'category_id' => '3'
        ]);
        DB::table('types')->insert([
            'name' => 'Preservativos',
            'category_id' => '3'
        ]);
    }
}
