<?php

use Illuminate\Database\Seeder;

class ImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('images')->insert([
            'path' => '1.jpg',
            'product_id' => '1'
        ]);
        DB::table('images')->insert([
            'path' => '2.jpg',
            'product_id' => '1'
        ]);
        DB::table('images')->insert([
            'path' => '3.jpg',
            'product_id' => '1'
        ]);
        DB::table('images')->insert([
            'path' => '4.jpg',
            'product_id' => '2'
        ]);
        DB::table('images')->insert([
            'path' => '5.jpg',
            'product_id' => '3'
        ]);
        DB::table('images')->insert([
            'path' => '6.jpg',
            'product_id' => '4'
        ]);
        DB::table('images')->insert([
            'path' => '7.jpg',
            'product_id' => '5'
        ]);
        DB::table('images')->insert([
            'path' => '8.jpg',
            'product_id' => '5'
        ]);
        DB::table('images')->insert([
            'path' => '9.jpg',
            'product_id' => '5'
        ]);
        DB::table('images')->insert([
            'path' => '10.jpg',
            'product_id' => '6'
        ]);
        DB::table('images')->insert([
            'path' => '11.jpg',
            'product_id' => '6'
        ]);
        DB::table('images')->insert([
            'path' => '12.jpg',
            'product_id' => '6'
        ]);
        DB::table('images')->insert([
            'path' => '13.jpg',
            'product_id' => '7'
        ]);
        DB::table('images')->insert([
            'path' => '14.jpg',
            'product_id' => '8'
        ]);
        DB::table('images')->insert([
            'path' => '15.jpg',
            'product_id' => '9'
        ]);
        DB::table('images')->insert([
            'path' => '16.jpg',
            'product_id' => '9'
        ]);
        DB::table('images')->insert([
            'path' => '17.jpg',
            'product_id' => '9'
        ]);
    }
}
