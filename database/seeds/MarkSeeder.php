<?php

use Illuminate\Database\Seeder;

class MarkSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $marks = ['SEXTO SENTIDO', 'ULTRA MUJER', 'PHEROMONAS', 'SEXITIVE', 'MISS V'];

        foreach ($marks as $item ) {
            DB::table('marks')->insert([
                'name' => $item
            ]);
        }






    }
}
