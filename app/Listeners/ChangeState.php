<?php

namespace App\Listeners;

use App\Event\SetExpired;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Set;
use Carbon\Carbon;

class ChangeState
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SetExpired  $event
     * @return void
     */
    public function handle(SetExpired $event)
    {
        foreach ($event->sets as $set){
            if ($set->expiration < Carbon::now()->toDateString()){
                $set->state ="inactive";
                $set->save();
            }
        }
    }
}
