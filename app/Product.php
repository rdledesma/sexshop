<?php

namespace App;
use App\Type;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = [
        'name',
        'price',
        'quantity',
        'description',
        'review',
        'measurement',
        'isOffer',
        'isRecommended',
        'discountPorcent',
        'showFlayer',
        'showCarousel',
        'type_id',
        'mark_id'
    ];

    public function mark(){
        return $this->belongsTo('App\Mark');
    }

    public function type(){
        return $this->belongsTo('App\Type');
    }

    public function images(){
        return $this->hasMany('App\Image');
    }



    public function set(){
        return $this->belongsToMany('App\Set') ;
    }

    public function scopeCategory($query, $category){
        if($category){
            $types = Type::where('category_id', $category)->select('id')->get();
            return $query->whereIn('type_id', $types);
        }
    }
    public function scopeType($query, $type){
        if($type){
            return $query->where('type_id', $type);
        }
    }

}
