<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSet extends Model
{
    protected $table="product_set";

    protected $fillable= ['product_id','set_id'];
}
