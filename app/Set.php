<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Set extends Model
{
    protected $fillable = [
        'name',
        'discount',
        'price',
        'expiration',
        'description'
    ];

    public function products(){
        return $this->belongsToMany('App\Product');

    }
}
