<?php

namespace App\Http\Controllers;

use App\Set;
use App\Product;
use App\ProductSet;
use Illuminate\Http\Request;
use Redirect;
use DB;
use Illuminate\Support\Collection as Collection;
use Carbon\Carbon;
use App\Event\SetExpired;

class SetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sets = Set::where('state','active')->orderBy('name')->get();
        event(new SetExpired($sets));
        return view('admin/sets/index', compact('sets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $products = Product::where('state','active')->orderBy('name')->get();
        return view('admin/sets/create', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:sets,name',
            'discount' => 'required',
            'expiration' => 'required',
        ]);

        $newSet = new Set;
        $newSet->name = $request->get('name');
        $newSet->discount = $request->get('discount');
        $mytime1 = Carbon::createFromFormat('Y-m-d',$request->get('expiration'));
        $newSet->expiration = $mytime1->toDateString();
        $newSet->description = $request->get('description');
        $newSet->save();
        $idset = $newSet->id;
        $products = $request->products;
        $cont = 0;
        $subtotal = 0;
        while ( $cont < count($products) ) {
            $productSet = new ProductSet();
            $pos=strpos($products[$cont],'_'); 
            $idproduct=substr($products[$cont],0, $pos); 
            $productSet->product_id = $idproduct;
            $productSet->set_id = $newSet->id;
            $prodprice=substr($products[$cont],$pos, $pos+1); 
            $prodprice=(double)substr($products[$cont],$pos+1);
            $subtotal = $subtotal + $prodprice;
            $cont = $cont+1;
            $productSet->save(); 
        }
        $discount = ($subtotal * $newSet->discount) / 100;
        $newSet->price = $subtotal - $discount;
        $newSet->save();
        flash("Nuevo combo añadido")->success();
        return Redirect::to('admin/sets');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Set  $set
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $set = Set::findOrFail($id);
        return view('admin/sets/show', compact('set'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Set  $set
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        $set = Set::with('products')->where('id',$id)->firstOrFail();
        
        //ProductSet::where('set_id',$id)->get();
        $productsel=$set->products;  
        $products = Product::where('state','active')->orderBy('name')->get();
        $productos = $products->diff($productsel);
        //dd($products);
        return view('admin/sets/edit', compact('set','productos','productsel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Set  $set
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:sets,name,'.$id,
            'discount' => 'required',
            'expiration' => 'required',
        ]);

        $set = Set::findOrFail($id);
        $id= $set->products->pluck('id');
        DB::table('product_set')->where('set_id',$set->id)->delete();
        $set->fill($request->all());
        /* $set->name = $request->get('name');
        $set->discount = $request->get('discount');
        $mytime1 = Carbon::createFromFormat('Y-m-d',$request->get('expiration'));
        $set->expiration = $mytime1->toDateString();
        $set->description = $request->get('description');*/
        //$set->update();
        $idset = $set->id;
        $products = $request->products;
        $cont = 0;
        $subtotal = 0;
        while ( $cont < count($products) ) {
            $productSet = new ProductSet();
            $pos=strpos($products[$cont],'_'); 
            $idproduct=substr($products[$cont],0, $pos);
            $productSet->product_id = $idproduct;
            $productSet->set_id = $set->id;
            $prodprice=substr($products[$cont],$pos, $pos+1); 
            $prodprice=(double)substr($products[$cont],$pos+1);
            $subtotal = $subtotal + $prodprice;
            $cont = $cont+1;
            $productSet->save(); 
        }
        $discount = ($subtotal * $set->discount) / 100;
        $set->price = $subtotal - $discount; 
        $set->save();
        
        flash("Combo actualizado correctamente")->success();
        return Redirect::to('admin/sets');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Set  $set
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $set = Set::findOrFail($id);
        $set->state = 'inactive';
        $set->update();
        flash("Combo eliminado")->error();
        return Redirect::to('admin/sets');
    }
}
