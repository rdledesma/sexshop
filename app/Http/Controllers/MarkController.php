<?php

namespace App\Http\Controllers;

use App\Mark;
use Illuminate\Http\Request;
use Redirect;

class MarkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $marks = Mark::where('state','active')->orderBy('name')->get();

        return view('admin/marks/index', compact('marks'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin/marks/create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newMark = new Mark($request->all());
        $newMark->save();
        flash("Nueva marca añadida")->success();
        return Redirect::to('admin/marks');

    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Mark  $mark
     * @return \Illuminate\Http\Response
     */
    public function show(Mark $mark)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Mark  $mark
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mark = Mark::findOrFail($id);
        return view('admin/marks/edit', compact('mark'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Mark  $mark
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $mark = Mark::findOrFail($id);
        $mark->update($request->all());

        $mark->save();
        flash("Marca actualizada correctamente")->success();
        return Redirect::to('admin/marks');

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Mark  $mark
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mark = Mark::findOrFail($id);
        $mark->state = 'inactive';
        $mark->update();
        flash("Marca eliminada")->error();
        return Redirect::to('admin/marks');

    }
}
