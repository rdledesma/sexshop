<?php

namespace App\Http\Controllers;

use App\Product;
use App\Type;
use App\Mark;
use App\Category;
use App\Image;


use Storage;
use Illuminate\Support\Facades\File;

use Illuminate\Http\Request;
use Redirect;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::where('state','active')->orderBy('name')->get();

        return view('admin/products/index', compact('products'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types = Type::where('state','active')->orderBy('name')->get();
        $marks = Mark::where('state','active')->orderBy('name')->get();
        $categories = Category::where('state','active')->orderBy('name')->get();
        return view('admin/products/create', compact(['types','marks','categories']));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:products,name',
            'price' => 'required',
            'quantity' => 'required',
            'measurement' => 'required',
            'photos.*' => 'image|mimes:jpeg,png,jpg,gif,svg'

        ]);

        $data = $request->all();
        $newProduct = new Product($request->all());
        $newProduct->save();

        $files = $request->file('photos');
        $destinationPath = 'image/'; // upload path
        $index = 1;
        if (!empty($files)) {
            foreach ($files as $item ) {

                $productImage = $item->getClientOriginalName();
                $item->move($destinationPath, $productImage);
                $newImage = new Image([
                    'path' => $productImage,
                    'product_id' => $newProduct->id
                ]);
                $newImage->save();
                $index =+1;
            }

        }
        flash('Nuevo producto añadido')->success();
        return Redirect::to('admin/products');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        return view('admin/products/show', compact('product'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$product = Product::with('images','type','mark')->where('id',$id)->firstOrFail();
        $product = Product::findOrFail($id);
        $types = Type::where('state','active')->orderBy('name')->get();
        $marks = Mark::where('state','active')->orderBy('name')->get();
        //dd($product->images);
        return view('admin.products.edit',compact('product','types','marks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|unique:products,name,'.$id,
            'price' => 'required',
            'quantity' => 'required',
            'measurement' => 'required',
            'photos.*' => 'image|mimes:jpeg,png,jpg,gif,svg'

        ]);
        $product = Product::findOrFail($id);
        $data = $request->all();
        $product->update($data);
        $product->save();
        $files = $request->file('photos');
        $destinationPath = 'image/'; // upload path
        $index = 1;
        if (!empty($files)) {
            foreach ($files as $item ) {
                $productImage = $item->getClientOriginalName();
                $item->move($destinationPath, $productImage);
                $newImage = new Image([
                    'path' => $productImage,
                    'product_id' => $product->id
                ]);
                $newImage->save();
                $index =+1;
            }
        }
        flash('Producto actualizado correctamente')->success();
        return Redirect::to('admin/products');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->state = 'inactive';
        $product->update();
        flash('Producto eliminado')->error();
        return Redirect::to('admin/products');

    }

    public function deleteimage($id){
        $image = Image::find($id);
        $file = 'image/'.$image->path;
        //dd($archivo);
        $delete = File::delete($file);
        $image->delete();
        //return back();
        return $delete;
    }
}
