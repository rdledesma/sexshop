<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;


class ShopController extends Controller
{
    public function index(Request $request)
    {
        //dd($request);
        $category = $request->get('category_id');
        $type = $request->get('type_id');



        $products = Product::where('state','active')
            ->category($category)
            ->type($type)
            ->orderBy('name')
            ->with('mark')
            ->with('type')
            ->with('images')
            ->get();

        return $products;
    }

    public function show($id)
    {
        $product = Product::where('id',$id)->with('images')->with('mark')->first();

        \MercadoPago\SDK::setAccessToken('TEST-4803844630692204-061918-192a9b06a7a549af709289aba88c87ae-246624539');

        // Crea un objeto de preferencia
        $preference = new \MercadoPago\Preference();

        // Crea un ítem en la preferencia
        $item = new \MercadoPago\Item();
        $item->title = 'Mi producto';
        $item->quantity = 1;
        $item->unit_price = 75.56;
        $preference->items = array($item);
        $preference->save();

        $product["preference"] = $preference;

        return $product;
    }


    public function pay($id){
        $product = Product::findOrFail($id);
        // Agrega credenciales
        \MercadoPago\SDK::setAccessToken('TEST-4803844630692204-061918-192a9b06a7a549af709289aba88c87ae-246624539');

        // Crea un objeto de preferencia
        $preference = new \MercadoPago\Preference();

        // Crea un ítem en la preferencia
        $item = new \MercadoPago\Item();
        $item->title = $product->name;
        $item->quantity = 1;
        $item->unit_price = $product->price;
        $preference->items = array($item);
        $preference->save();

        return view('pay/payProcess', compact('preference'));
    }
}
