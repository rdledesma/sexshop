$( function() {
    $("#isOffer").change( function() {
        if ($(this).val() === "0") {
            $("#discountPorcent").prop("disabled", true);
        } else {
            $("#discountPorcent").prop("disabled", false);
        }
    });
});

$('div.alert').not('.alert-important').delay(3000).fadeOut(350);

$('.deletebtn').click(function(){
    var id = $(this).attr("data-id");
    $.ajax({
        url: '/admin/products/deleteimage/' + id,
        //type: 
        //data: 
        success: function(response){
            var element = document.getElementById('ideimagen-'+id);
            //console.log(element);
            element.parentNode.removeChild(element);
        }
    });
});
