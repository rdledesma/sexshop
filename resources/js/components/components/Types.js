import React, {useState} from 'react'

export default function Types(props) {
    const {types, setType, filterByType} = props;

    const [selectedTypes, setSelectedTypes] = useState([]);

    const selected = (id) => {
        var selecteds = selectedTypes;
        var n = selectedTypes.includes(id);
        if(!n){
            setSelectedTypes([id]);
            setType(id);
            filterByType(id);
        }
        else{
            selecteds = selecteds.filter((item) => item !== id);
            setSelectedTypes([...selecteds])
            setType(null);
            filterByType('');

        }

    }



    return (
        <div>
            { types &&
                    types.map(item =>
                    <div style={{display:"inline"}}>
                        <button className="btn btn-sm" style={{opacity: selectedTypes.includes(item.id) ? 1: 0.5}} onClick={() => selected(item.id)}> {item.name}</button>
                    </div>
                )}
        </div>
    )
}
