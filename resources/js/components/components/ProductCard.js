import React, {useState} from 'react'
import Card from 'react-bootstrap/Card'
import { Link } from 'react-router-dom';
import { useHistory } from "react-router-dom";

export default function ProductCard(props) {
    const {item} = props;

    const [image, setImage] = useState('image/'+item.images[0].path);
    let history = useHistory();


    const changeImage =() => {

        if(item.images[1]){
            setImage('image/'+item.images[1].path)
        }
    }


    const toProduct = () => {
        history.push('product/'+item.id, {...props})
    }


    return(

            <Card onClick={() => toProduct()} onMouseEnter={() => changeImage()} onMouseOut={() => setImage('image/'+item.images[0].path)}>
                <Card.Img variant="top" src= {image} />
                <Card.Body>
                {item.isOffer ?
                    <p className="text-danger font-weight-bold">{item.discountPorcent}% Descuento</p>
                : null}
                <Card.Subtitle>{item.type.name}</Card.Subtitle>
                <Card.Title className="font-weight-bold">{item.name}</Card.Title>
                <Card.Subtitle>{item.mark.name}</Card.Subtitle>
                <Card.Text>
                    {item.description.slice(0,100)}...
                </Card.Text>

                </Card.Body>
            </Card>
    )
}
