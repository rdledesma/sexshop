import React, { useState } from 'react'

export default function Menu() {

    const [showFilter, setShowFilter] = useState(false);

    return (
        <div className="container">
                <div className="col-2">
                    <button className="btn" onClick={() => setShowFilter(!showFilter)}>Ordenar</button>
                </div>
                {
                    showFilter &&
                    <div className="col-6" style={{display: "inline-block"}}>
                    <p style={{display: "inline"}}>Nombre: A-Z </p>
                    <p style={{display: "inline"}}>Nombre: Z-A </p>
                    <p style={{display: "inline"}}>Menor Precio </p>
                    <p style={{display: "inline"}}>Mayor Precio </p>
                    </div>
                }
            </div>
    )
}
