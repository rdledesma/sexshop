import React, {useEffect, useState} from 'react'

export default function Alert(props) {
    const random = Math.random();
    const [seconds, setSeconds] = useState(0);
    const [show, setShow] = useState(true);
    const {products} = props;

    useEffect(() => {
    const interval = setInterval(() => {
      setSeconds(seconds => seconds + 1);
    }, 500);
    return () => {
        clearInterval(interval);
        setShow(false)
    };
  }, []);



    if(seconds>50 && seconds%500<50 ){
        return (

            <div class="container alert alert-dark alert-dismissible fade show fixed-bottom" role="alert">
                <strong>Date prisa! </strong> Acabamos de vender un { products && products.length>0 ? products[Math.floor(products.length/2)].name : null}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

        )
    }
    else{
        return null;
    }

}
