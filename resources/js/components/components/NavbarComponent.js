import React, { useState, useEffect} from 'react'
import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import NavDropdown from 'react-bootstrap/NavDropdown'



export default function NavbarComponent(props) {

    const {filterByCategory, products, setProducts, setCategory, setTypes}  = props

    const [categories, setCategories] = useState([]);
    const getCategories = async () => {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json',
            }
          };
        const data = await axios.get('/api/shop/categories', axiosConfig);
        const misdatos = data.data;

        setCategories(misdatos)

    }

    useEffect(() => {
        getCategories();
    }, [])


    const filtar = (item) => {
        setTypes(item.types);
        setCategory(item.name)
        filterByCategory(item.id);
    }


    return (
        <div className="fixed-top"  style={{padding:'5px', backgroundColor:"black"}}>
            <Nav activeKey="/">
            <Nav.Item>
                <Nav.Link href="/" className="text-light" >Home</Nav.Link>
            </Nav.Item>
            {categories && categories.map(item =>
                    <Nav.Item key={item.id}>
                        <Nav.Link className="text-light" onClick={() => filtar(item)} > {item.name}</Nav.Link>
                    </Nav.Item>
                )}
            <Nav.Item style={{backgroundColor:"green"}}>
                <Nav.Link href="/" className="text-light">Whatsapp</Nav.Link>
            </Nav.Item>
            <Nav.Item>
                <Nav.Link href="/" className="text-light">BLOG</Nav.Link>
            </Nav.Item>
        </Nav>

        </div>
    )
}
