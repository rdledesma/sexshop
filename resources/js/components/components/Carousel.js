import React, {useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios'

import CarouselComponent from 'react-bootstrap/Carousel'
export default function Carousel() {

    const [products, setProducts] = useState([]);

    const getProducts = async () => {
        let axiosConfig = {
            headers: {
                'Content-Type': 'application/json',
            }
          };
        const data = await axios.get('/api/shop/products', axiosConfig);
        const misdatos = data.data;

        setProducts(misdatos)
    }

    useEffect(() => {
        getProducts();
    }, [])

    return (
        // <CarouselComponent itemsToShow={1} transitionMs={500} enableAutoPlay={true} disableArrowsOnEnd={false}>
        //     {products && products.map(item => <img src= {'image/'+item.images[0].path} />)}
        // </CarouselComponent>
        <CarouselComponent className="text-center">
        {products && products.map(item =>

            <CarouselComponent.Item>
            <img
                src= {'image/'+item.images[0].path}
                alt="First slide"
                height="20%"
                />
        </CarouselComponent.Item>)}

      </CarouselComponent>
    );
}
