import React, {useState, useEffect} from 'react'
import { Link } from 'react-router-dom';
import CardColumns from 'react-bootstrap/CardColumns'
import Axios from 'axios';
import ProductCard from '../components/ProductCard'
import { keyBy } from 'lodash';
import Carousel from '../components/Carousel';
import Alert from '../components/Alert';




  export default function Landing (props) {

    const {products, category } = props;


    return (
        <div className="text-center container mt-5" style={{backgroundColor:'white'}}>


            {category ? <h1>{category}</h1> :
                <div className="row d-flex justify-content-center">
                    <div className="col-12" >
                        <Carousel></Carousel>
                    </div>
                    <h1>Catálogo</h1>
                </div>

            }
            {
                products &&
                <CardColumns>
                {products && products.map(item =>
                    <ProductCard item={item} key={item.id} />
                )}
                </CardColumns>
            }
            <Alert products={products}></Alert>
        </div>
    )
}
