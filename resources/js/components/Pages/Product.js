import React, { useState, useEffect } from 'react'
import Axios from 'axios';

import Carousel from 'react-elastic-carousel'

export default function Product(props) {

    const [loading, setLoading] = useState(false);
    const [product, setProduct] = useState(null);
    const [images, setImages] = useState([])

    const getProduct = async () => {
        setLoading(true)
        const data = await Axios.get('/api/shop/product/'+props.match.params.id);
        console.log(data);
        setImages(data.data.images);

        setProduct(data.data)
        setLoading(false)
    }

    useEffect(() => {
        getProduct();
    }, [])

    if (loading || !product) {
        return <h1 className="text-center">cargando..!</h1>
    }

    return (
        <div className="row">
            <div className="col-sm">
                <Carousel className="">
                    {images.map((elem)=> <img
                        src= {'../image/'+elem.path}
                        alt="First slide"

                    />)}
                </Carousel>
            </div>
            <div className="m-5 col-sm text-justify">
                <h4>{product.name}</h4>
                <p>Marca: <strong>{product.mark.name}</strong></p>
                <p><strong>Precio: </strong> <strong className="h4">${product.price}</strong></p>
                <p>
                    {product.description}
                </p>
                <p>
                    <i>{product.review}</i>
                </p>
                <p>
                    <strong>Presentación: </strong>{product.measurement}
                </p>

                <form action={`/pago/`+product.id} method="GET"  >
                    <button type="submit" className="btn btn-dark">Comprar este artículo</button>
                </form>
            </div>
        </div>
    )
}
