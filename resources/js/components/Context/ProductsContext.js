import React, { useState, useEffect, useMemo} from 'react';
import Axios from 'axios';
const ProductsContext =  React.createContext();

export function ProductsProvider(props){
    const [products, setProducts] = useState([]);
    const [category, setCategory] = useState(null);
    const [type, setType] = useState(null);
    const [types, setTypes] = useState([]);



    const getProducts = async () => {

        const data = await Axios.get('/api/shop/products');

        setProducts(data.data);
    }

    useEffect(() => {
        getProducts();
    }, [])

    const filterByCategory = async (id) => {

        const data = await Axios.get('/api/shop/products', {
            params: {
              category_id: id
            }
          });
        setProducts(data.data);
    }

    const filterByType = async (id) => {

        const data = await Axios.get('/api/shop/products', {
            params: {
              type_id: id
            }
          });
        setProducts(data.data);
    }


    const value = useMemo(() => ({
        products,
        setProducts,
        filterByCategory,
        category,
        setCategory,
        types,
        setTypes,
        type,
        setType,
        filterByType
      }), [products, setProducts,filterByCategory, filterByType]);


    return <ProductsContext.Provider value={value} {...props} />;
}

export function useProducts(){
    const context = React.useContext(ProductsContext);
    if(!context){
        throw new Error('no está en el proveedor');
    }

    return context;
}
