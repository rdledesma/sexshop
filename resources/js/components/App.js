import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios'


import Carousel from './components/Carousel';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';


import LandingPage from './Pages/Landing';
import ProductPage from './Pages/Product';
import Navbar from './components/NavbarComponent';
import Types from './components/Types';

import {ProductsProvider, useProducts} from './Context/ProductsContext'

export default () => (

      <App />

  );

function App() {

    const {
        products,
        setProducts,
        filterByCategory,
        filterByType,
        category,
        setCategory,
        types,
        setTypes,
        type,
        setType
    }  = useProducts();


    return (
        <Router>

            <Navbar
                products={products}
                setProducts={setProducts}
                setCategory={setCategory}
                filterByCategory={filterByCategory}
                setTypes={setTypes}
                type={type}
                >
            </Navbar>
            <div className="row mt-5">
                <div className="col text-center">
                <h1>Mdtiendavirtual</h1>
                <Types types={types} setType={setType} filterByType={filterByType}></Types>
                </div>
            </div>



            <Switch>

            <Route
                path="/product/:id"
                render={(props)=><ProductPage {...props} />}>
            </Route>
            <Route
                render={(props)=><LandingPage {...props} products={products} category={category}/>}>
            </Route>

            </Switch>
        </Router>
    );
}

//export default App;



if (document.getElementById('root')) {
    ReactDOM.render(<ProductsProvider>
        <App />
      </ProductsProvider>, document.getElementById('root'));
}
