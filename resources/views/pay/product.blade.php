<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Pagar</title>
</head>
<body>
    <h1>A pagar</h1>
    <form action="/procesar-pago" method="POST">

        <script
         src="https://www.mercadopago.com.ar/integrations/v1/web-payment-checkout.js"
         data-preference-id="{{$preference->id}}">
        </script>
    </form>
</body>
</html>
