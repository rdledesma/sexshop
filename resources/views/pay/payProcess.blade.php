@extends('layouts.app')

@section('content')

<div class="container text-center col-3">
    <form action="/procesar-pago" method="POST">
        <h3>Estás a punto de iniciar el pago correspondiente a:</h3>
        <table class="table table-sm">
            <thead class="thead-dark">
              <tr>
                <th>Nombre</th>
                <th>Precio</th>
              </tr>
            </thead>
            <tbody>
                @foreach ($preference->items as $item)

                <tr>
                    <td>{{$item->title}}</td>
                    <td>{{$item->unit_price}}</td>
                </tr>
                @endforeach


            </tbody>
          </table>

        <script
         src="https://www.mercadopago.com.ar/integrations/v1/web-payment-checkout.js"
         data-preference-id="{{$preference->id}}">
        </script>
      </form>
</div>
@endsection
