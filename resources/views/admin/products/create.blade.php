@extends('admin.layouts.admin')

@section('contenido')


<div class="row m-4">
    <div class="col">
        <h1>Nuevo Producto</h1>
    </div>
</div>
<form action="{{ route('admin.products') }}" method="POST" enctype="multipart/form-data" >
<div class="row m-4">




            {{ csrf_field() }}
            <div class="form-group col-6">
              <label for="exampleInputEmail1">Nombre</label>
              <input type="name" class="form-control" name="name" required>
            </div>

            <div class="form-group col-3">
                <label for="exampleInputEmail1">Marca</label>
                <select class="form-control" name="mark_id" required>
                    @foreach ($marks as $mark)
                        <option value="{{$mark->id}}">{{$mark->name}}</option>
                    @endforeach
                </select>
            </div>


            <div class="form-group col-3">
                <label for="exampleInputEmail1">Tipo</label>
                <select class="form-control" name="type_id" required>
                    @foreach ($types as $type)
                        <option value="{{$type->id}}" >{{$type->name}}</option>
                    @endforeach
                </select>
            </div>


            <div class="form-group col-2">
                <label for="exampleInputEmail1">Precio</label>
                <input type="name" class="form-control" name="price" required>
            </div>

            <div class="form-group col-2">
                <label for="exampleInputEmail1">Cantidad en stock</label>
                <input type="name" class="form-control" name="quantity" required>
            </div>
            <div class="form-group col-2">
                <label for="exampleInputEmail1">Med. de Presentación</label>
                <input type="name" class="form-control" name="measurement" required>
            </div>

            <div class="form-group col-2">
                <label for="exampleInputEmail1">Está en oferta?</label>
                <select class="form-control" name="isOffer" id= "isOffer" required>
                    <option value="0">NO</option>
                    <option value="1">SI</option>

                </select>
                <label for="exampleInputEmail1">% de descuento</label>
                <input type="name" class="form-control" name="discountPorcent" id= "discountPorcent" disabled>
            </div>

            <div class="form-group col-2">
                <label for="exampleInputEmail1">Mostrar en anuncios?</label>
                <select class="form-control" name="showFlayer" required>
                    <option value="0">NO</option>
                    <option value="1">SI</option>

                </select>
            </div>

            <div class="form-group col-2">
                <label for="exampleInputEmail1">Mostrar en principal?</label>
                <select class="form-control" name="showCarousel" required>
                    <option value="1">SI</option>
                    <option value="0">NO</option>
                </select>
            </div>


            <div class="form-group col-8">
                <label for="exampleInputEmail1">Descripción</label>
                <textarea type="name" class="form-control" name="description" required></textarea>
            </div>
            <div class="form-group col-8">
                <label for="exampleInputEmail1">Reseña</label>
                <textarea type="name" class="form-control" name="review" required ></textarea>
            </div>

            <div class="form-group col-5">
                <label for="exampleInputEmail1">Imágen del Producto</label>
                <input type="file" class="form-control" name="photos[]" multiple="multiple" />
            </div>

            <div class="form-group col-12">
                <a href="{{ route('admin.products') }}" class="btn btn-secondary">Cancelar</a>
                <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
</div>
</form>

@endsection
