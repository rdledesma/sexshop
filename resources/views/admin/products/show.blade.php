@extends('admin.layouts.admin')

@section('contenido')

    <div class="row m-4">
        <div class="col">
            <h1 class="text-primary">{{$product->name}}</h1>
        </div>
    </div>

    <div class="row m-4">
                <div class="form-group col-3">
                    <label for="exampleInputEmail1">Marca: </label>
                    <label ><p class="text-primary">{{$product->mark->name}}<p></label>
                </div>

                <div class="form-group col-3">
                    <label for="exampleInputEmail1">Tipo:</label>
                    <label ><p class="text-primary">{{$product->type->name}}<p></label>
                </div>

                <div class="form-group col-2">
                    <label for="exampleInputEmail1">Precio:</label>
                    <label ><p class="text-primary">${{$product->price}}<p></label>
                </div>

                <div class="form-group col-2">
                    <label for="exampleInputEmail1">Cantidad en stock:</label>
                    <label ><p class="text-primary">{{$product->quantity}}<p></label>
                </div>
                <div class="form-group col-2">
                    <label for="exampleInputEmail1">Med. de Presentación:</label>
                    <label ><p class="text-primary">{{$product->measurement}}<p></label>
                </div>

                <div class="form-group col-3">
                    <label for="exampleInputEmail1">Está en oferta?</label>
                    <p class="text-primary">{{$product->isOffer=== 0 ? 'NO' : 'SI'  }}<p>
                </div>

                <div class="form-group col-3">
                    <label for="exampleInputEmail1">Porcentaje de descuento:</label>
                    <p class="text-primary">{{$product->discountPorcent}}%<p>
                </div>

                <div class="form-group col-2">
                    <label for="exampleInputEmail1">Se muestra en anuncios?</label>
                    <p class="text-primary">{{$product->showFlayer!== 0 ? 'SI' : 'NO'  }}<p>
                </div>

                <div class="form-group col-2">
                    <label for="exampleInputEmail1">Se muestra en principal?</label>
                    <p class="text-primary">{{$product->showCarousel!== 0 ? 'SI' : 'NO'  }}<p>
                </div>

                <div class="form-group col-12">
                    <label for="exampleInputEmail1">Descripción:</label>
                    <label>
                        <p>{{$product->description}}</p>
                    </label>
                </div>

                <div class="form-group col-10">
                    <label for="exampleInputEmail1">Reseña:</label>
                    <label>
                        <p>{{$product->review}}</p>
                    </label>
                </div>

                <div class="form-group col-12">
                    @if ( count ( $product->images )!=0 )
                        <label for="exampleInputEmail1">Imágen(es):</label>
                        <div class="row">
                            @foreach ($product->images as $image)                                        
                                <div class="col-sm-2">
                                    <img style="width:150px; height:150px;" id="idimagen-{{$image->id}}" src="{{asset('image/'.$image->path)}}" class="img-fluid mb-2" />
                                </div>
                            @endforeach
                        </div>
                    @endif  
                </div>

                <div class="form-group col-12">
                    <a href="{{ route('admin.products') }}" class="btn btn-secondary">Volver</a>
                </div>
    </div>

@endsection
