@extends('admin.layouts.admin')
<style>
    .button-container{
        display:inline-block;
        position:relative;
        }
        #boton{
            position: absolute;
            right:1em;
            border-radius:1.5em;
            padding:0.5em 1em;
        }
</style>

@section('contenido')
<div class="row">
    <div class="col">
        <h1>Editar Producto</h1>
    </div>
</div>

<div class="row">
        <form action="{{ route('admin.products.update',$product->id) }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            @method('PUT')
            <div class="row m-4">
                    <div class="form-group col-6">
                    <label for="exampleInputEmail1">Nombre</label>
                    <input type="name" class="form-control" name="name" value="{{ $product->name}}" required>
                    </div>

                    <div class="form-group col-3">
                        <label for="exampleInputEmail1">Marca</label>
                        <select class="form-control" name="mark_id">
                            @foreach ($marks as $mark)
                                @if ($mark->name == $product->mark->name)
                                    <option value="{{ $mark->id }}" selected="selected">{{ $mark->name }}</option>
                                @else
                                    <option value="{{ $mark->id }}">{{ $mark->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-3">
                        <label for="exampleInputEmail1">Tipo</label>
                        <select class="form-control" name="type_id">
                            @foreach ($types as $type)
                                @if ($type->name == $product->type->name)
                                    <option value="{{$type->id}}" selected="selected">{{$type->name}}</option>
                                @else
                                    <option value="{{$type->id}}" >{{$type->name}}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-2">
                        <label for="exampleInputEmail1">Precio</label>
                        <input type="name" class="form-control" name="price" value="{{ $product->price}}" required>
                    </div>

                    <div class="form-group col-2">
                        <label for="exampleInputEmail1">Cantidad en stock</label>
                        <input type="name" class="form-control" name="quantity" value="{{ $product->quantity}}" required> 
                    </div>
                    <div class="form-group col-2">
                        <label for="exampleInputEmail1">Med. de Presentación</label>
                        <input type="name" class="form-control" name="measurement" value="{{ $product->measurement}}" required>
                    </div>

                    <div class="form-group col-2">
                        <label for="exampleInputEmail1">Está en oferta?</label>
                        <select class="form-control" name="isOffer" id= "isOffer">
                            @if ($product->isOffer == "0")
                                <option value="0" selected="selected">NO</option>
                                <option value="1">SI</option>
                            @else
                                <option value="0">NO</option>
                                <option value="1" selected="selected">SI</option>
                            @endif
                        </select>
                        <label for="exampleInputEmail1">% de descuento</label>
                        @if ($product->isOffer == "0")
                            <input type="name" class="form-control" name="discountPorcent" disabled id="discountPorcent">
                        @else
                            <input type="name" class="form-control" name="discountPorcent" value="{{ $product->discountPorcent}}" id="discountPorcent">
                        @endif
                    </div>

                    <div class="form-group col-2">
                        <label for="exampleInputEmail1">Mostrar en anuncios?</label>
                        <select class="form-control" name="showFlayer">
                            @if ($product->showFlayer == "0")
                                <option value="0" selected="selected">NO</option>
                                <option value="1">SI</option>
                            @else
                                <option value="0">NO</option>
                                <option value="1" selected="selected">SI</option>
                            @endif

                        </select>
                    </div>

                    <div class="form-group col-2">
                        <label for="exampleInputEmail1">Mostrar en principal?</label>
                        <select class="form-control" name="showCarousel">
                            @if ($product->showCarousel == "0")
                                <option value="0" selected="selected">NO</option>
                                <option value="1">SI</option>
                            @else
                                <option value="0">NO</option>
                                <option value="1" selected="selected">SI</option>
                            @endif
                        </select>
                    </div>


                    <div class="form-group col-8">
                        <label for="exampleInputEmail1">Descripción</label>
                        <textarea type="name" class="form-control" name="description" rows="5" required >{!! $product->description !!}</textarea>
                    </div>
                    <div class="form-group col-8">
                        <label for="exampleInputEmail1">Reseña</label>
                        <textarea type="name" class="form-control" name="review" required>{!! $product->review !!}</textarea>
                    </div>

                    <div class="form-group col-5">
                        <label for="exampleInputEmail1">Imágen de Producto</label>
                        <input type="file" class="form-control" name="photos[]" multiple="multiple" />
                    </div>
                    
                    <div  class="form-group col-12">
                        @if ( count ( $product->images )!=0 )
                            <label for="exampleInputEmail1">Imágen(es) Actual(es):</label>
                            
                                <div class="row">
                                    @foreach ($product->images as $image)                                        
                                        <div id="ideimagen-{{$image->id}}"  class="col-sm-2">
                                            <div class=“button-container”>
                                                <img style="width:150px; height:150px;" id="idimagen-{{$image->id}}" src="{{asset('image/'.$image->path)}}" class="img-fluid mb-2" />
                                                <a class="btn btn-default deletebtn" data-id="{{ $image->id }}" id="boton">
                                                <i class="fas fa-times-circle" style="color:red;"></i></a>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
            
                        @else
                            Aún no se ha cargado una imágen para este producto
                        @endif  
                    </div>
                    <div class="form-group col-12">
                        <a href="{{ route('admin.products') }}" class="btn btn-secondary">Cancelar</a>
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>
            </div>
        </form>
</div>

@endsection

