@extends('admin.layouts.admin')

@section('contenido')


<div class="row">
    <div class="col">
        <h1>Listado de Productos</h1>
    </div>
    <div class="col">
        <a class="btn btn-primary" href="{{ route('admin.products.create') }}" >Agregar Nuevo Producto</a>
    </div>

</div>
<br>
<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">Nombre</th>
        <th scope="col">Acción</th>

      </tr>
    </thead>
    <tbody>
        @foreach ($products as $product)
        <tr>
            <th scope="row">{{$product->name}}</th>
            <td>
                <div>
                    <form action="{{ route('admin.products.delete', $product)}}" method="post" style= "margin-bottom: 0px;">
                        <a href="{{ route('admin.products.show', $product) }}" class="btn btn-secondary">Detalles</a>
                        <a href="{{ route('admin.products.edit', $product) }}" class="btn btn-warning">Editar</a>
                        {{ csrf_field() }}
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Eliminar</button>
                    </form>
                </div>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>

@endsection
