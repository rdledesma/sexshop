@extends('admin.layouts.admin')

@section('contenido')

<form action="/procesar-pago" method="POST">
    <script
     src="https://www.mercadopago.com.ar/integrations/v1/web-payment-checkout.js"
     data-preference-id="{{$preference->id}}">
    </script>
  </form>
@endsection
