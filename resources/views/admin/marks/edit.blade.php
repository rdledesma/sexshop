@extends('admin.layouts.admin')

@section('contenido')


<div class="row">
    <div class="col">
        <h1>Editar marca</h1>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <form action="{{ route('admin.marks.update',$mark->id) }}" method="POST">
            {{ csrf_field() }}
            @method('PUT')
            <div class="form-group">
              <label for="exampleInputEmail1">Nombre</label>
              <input type="name" class="form-control" name="name" placeholder="{{$mark->name}}" required>
            </div>
            <a href="{{ route('admin.marks') }}" class="btn btn-secondary">Cancelar</a>
            <button type="submit" class="btn btn-primary">Actualizar</button>
        </form>
    </div>
</div>



@endsection
