@extends('admin.layouts.admin')

@section('contenido')


<div class="row">
    <div class="col">
        <h1>Listado de marcas</h1>
    </div>
    <div class="col">
        <a class="btn btn-primary" href="{{ route('admin.marks.create') }}" >Agregar Nueva</a>
    </div>
</div>
<br>
<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">Nombre</th>
        <th scope="col">Acción</th>

      </tr>
    </thead>
    <tbody>
        @foreach ($marks as $mark)
        <tr>
            <th scope="row">{{$mark->name}}</th>
            <td>
                <div>
                    <form action="{{ route('admin.marks.delete', $mark)}}" method="post" style= "margin-bottom: 0px;">
                    {{ csrf_field() }}
                    @method('DELETE')
                    <button class="btn btn-danger" type="submit">Eliminar</button>
                    <a href="{{ route('admin.marks.edit', $mark) }}" class="btn btn-secondary">Editar</a>
                </form>

                </div>

            </td>
        </tr>
        @endforeach

    </tbody>
  </table>


@endsection
