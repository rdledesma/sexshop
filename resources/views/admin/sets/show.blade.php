@extends('admin.layouts.admin')

@section('contenido')


<div class="row m-4">
    <div class="col">
        <h1 class="text-primary">{{$set->name}}</h1>
    </div>
</div>

<div class="row m-4">

            <div class="form-group col-4">
                <label for="exampleInputEmail1">FECHA DE EXPIRACIÓN:</label>
                <label ><p class="text-primary"><?php $fv = new DateTime($set->expiration); echo $fv->format('d-m-Y');?><p></label>
            </div>
            
            <div class="form-group col-2">
                <label for="exampleInputEmail1">DESCUENTO:</label>
                <label ><p class="text-primary">{{$set->discount}}%<p></label>
            </div>

            <div class="form-group col-2">
                <label for="exampleInputEmail1">PRECIO:</label>
                <label ><p class="text-primary">${{$set->price}}<p></label>
            </div>

            <div class="form-group col-12">
                <label for="exampleInputEmail1">DESCRIPCIÓN: </label>
                <label>
                    <p>{{$set->description}}</p>
                </label>
            </div>

            <div class="form-group col-12">
                <label for="exampleInputEmail1">PRODUCTOS:</label>
                @foreach ($set->products as $prod)
                <div class="form-group  col-6">
                    <a href="{{ route('admin.products.show', $prod) }}">{{ $prod->name }}</a>
                </div>
                @endforeach
            </div>

            <div class="form-group col-12">
                <a href="{{ route('admin.sets') }}" class="btn btn-secondary">Volver</a>
            </div>
</div>

@endsection
