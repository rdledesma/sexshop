@extends('admin.layouts.admin')

@section('contenido')

<div class="row m-4">
    <div class="col">
        <h1>Editar Combo</h1>
    </div>
</div>
<form action="{{ route('admin.sets.update',$set->id) }}" method="POST">
    {{ csrf_field() }}
    @method('PUT')
    <div class="row m-4">
            <div class="form-group col-6">
              <label for="exampleInputEmail1">Nombre</label>
              <input type="name" class="form-control" name="name" value="{{$set->name}}" required>
            </div>

            <div class="form-group col-3">
                <label>Fecha de Expiración</label>
                    <input type ="date" name="expiration" value="{{$set->expiration}}" class="form-control" required>
            </div>

            <div class="form-group col-3">
                <label for="exampleInputEmail1"> Descuento</label>
                    <input type="number" step="any" class="form-control" name="discount" value="{{$set->discount}}" required>
            </div>

            <div class="form-group col-6">
                <label for="exampleInputEmail1">Descripción</label>
                <textarea type="name" class="form-control" name="description" required> {{ $set->description }}</textarea>
            </div>
    
            <div class="form-group col-6">
                <label for="idproduct">Productos</label>
                    <select id="product" name="products[]" multiple="multiple" class="form-control" required>
                        
                        @foreach ($productsel as $prod)
                            <option value="{{$prod->id}}_{{$prod->price}}" selected="selected">{{ $prod->name }}</option>
                        @endforeach
                        @foreach ($productos as $product)
                            <option value="{{$product->id}}_{{$product->price}}">{{$product->name}}</option>
                        @endforeach
                    </select>
                    <br>
            </div>
        
            <div class="form-group col-12">
                <a href="{{ route('admin.sets') }}" class="btn btn-secondary">Cancelar</a>
                <button type="submit" class="btn btn-primary" id="btn1" name="btn1">Guardar</button>
            </div>
</div>
</form>
<script>
    $('#product').select2({
});
</script>

@endsection
