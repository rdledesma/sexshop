@extends('admin.layouts.admin')

@section('contenido')


<div class="row">
    <div class="col">
        <h1>Listado de Combos</h1>
    </div>
    <div class="col">
        <a class="btn btn-primary" href="{{ route('admin.sets.create') }}" >Agregar Nuevo Combo</a>
    </div>
</div>
<br>
<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">Nombre</th>
        <th scope="col">Acción</th>
      </tr>
    </thead>
    <tbody>
        @foreach ($sets as $set)
        <tr>
            <th scope="row">{{$set->name}}</th>
            <td>
                <div>
                    <form action="{{ route('admin.sets.delete', $set)}}" method="post" style= "margin-bottom: 0px;">
                        <a href="{{ route('admin.sets.show', $set) }}" class="btn btn-secondary">Detalles</a>
                        <a href="{{ route('admin.sets.edit', $set) }}" class="btn btn-warning">Editar</a>

                        {{ csrf_field() }}
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Eliminar</button>
                    </form>
                </div>
            </td>
        </tr>
        @endforeach

    </tbody>
  </table>
  

@endsection
