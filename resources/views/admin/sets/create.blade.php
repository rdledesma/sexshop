@extends('admin.layouts.admin')

@section('contenido')

<div class="row m-4">
    <div class="col">
        <h1>Nuevo Combo</h1>
    </div>
</div>
<form action="{{ route('admin.sets') }}" method="POST">
    {{ csrf_field() }}
    <div class="row m-4">
            <div class="form-group col-6">
              <label for="exampleInputEmail1">Nombre</label>
              <input type="name" class="form-control" name="name" required>
            </div>

            <div class="form-group col-3">
                <label>Fecha de Expiración</label>	
                    <input type ="date" name="expiration" value="<?php echo date('Y-m-d');?>" class="form-control">
            </div>

            <div class="form-group col-3">
                <label for="exampleInputEmail1"> Descuento</label>
                    <input type="number" class="form-control" name="discount" required>
            </div>

            <div class="form-group col-6">
                <label for="exampleInputEmail1">Descripción</label>
                <textarea type="name" class="form-control" name="description" required></textarea>
            </div>
    
            <div class="form-group col-6">
                <label for="idproduct">Productos</label>
                    <select id="product" name="products[]" multiple="multiple" class="form-control" required>
                        @foreach ($products as $product)
                            <option value="{{$product->id}}_{{$product->price}}">{{$product->name}}</option>
                        @endforeach
                    </select>
                    <br>
            </div>
        
            <div class="form-group col-12">
                <a href="{{ route('admin.sets') }}" class="btn btn-secondary">Cancelar</a>
                <button type="submit" class="btn btn-primary" id="btn1" name="btn1">Guardar</button>
            </div>
</div>
</form>
<script>
    $('#product').select2({
});
</script>

@endsection
