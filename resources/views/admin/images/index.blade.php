@extends('admin.layouts.admin')

@section('contenido')


<div class="row">
    <div class="col">
        <h1>Galería de imágenes</h1>
    </div>
    <div class="col">
        <a class="btn btn-primary" href="{{ route('admin.images.create') }}" >Agregar Nueva</a>
    </div>
</div>

<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">Path</th>
        <th scope="col">Acción</th>

      </tr>
    </thead>
    <tbody>
        @foreach ($images as $image)
        <tr>
            <th scope="row">{{$image->path}}</th>
            <td>
                <div>
                   <form action="{{ route('admin.images.delete', $image)}}" method="post">
                    {{ csrf_field() }}
                    @method('DELETE')
                    <button class="btn btn-danger" type="submit">Eliminar</button>
                </form>

                </div>

            </td>
        </tr>
        @endforeach

    </tbody>
  </table>


@endsection
