@extends('admin.layouts.admin')

@section('contenido')


<div class="row">
    <div class="col">
        <h1>Nueva Imagen</h1>
    </div>
</div>

<div class="row">
    <div class="col-6">
        <form action="{{ route('admin.images') }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="exampleInputEmail1">Imagen</label>
              <input type="file" class="form-control" name="photos[]" multiple>
            </div>
            <a href="{{ route('admin.images') }}" class="btn btn-secondary">Cancelar</a>
            <button type="submit" class="btn btn-primary">Guardar</button>
        </form>
    </div>
</div>



@endsection
