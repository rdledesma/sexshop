@extends('layouts.app')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
@section('content')

    <div class="row justify-content-center">
        <div class="col-2">
            <ul class="list-group">
                <a href="{{ route('admin.categories') }}" ><li class="list-group-item">Categorías</li></a>
                <a href="{{ route('admin.marks') }}" ><li class="list-group-item">Marcas</li></a>
                <a href="{{ route('admin.products') }}" ><li class="list-group-item">Productos</li></a>
                <a href="{{ route('admin.sets') }}" ><li class="list-group-item">Combos</li></a>
            </ul>
        </div>
        <div class="col-10">
            
            @include('flash::message')

            @if ($errors->any())
                <div class="alert alert-danger">
                <ul>
                    @foreach($errors->all() as $error)
                        <li> {{ $error }} </li>
                    @endforeach
                </ul>
                </div> 
            @endif

            @yield('contenido')

        </div>
    </div>

@endsection
