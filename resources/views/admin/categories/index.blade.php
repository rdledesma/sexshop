@extends('admin.layouts.admin')

@section('contenido')


<div class="row">
    <div class="col">
        <h1>Listado de categorías</h1>
    </div>
    <div class="col">
        <a class="btn btn-primary" href="{{ route('admin.categories.create') }}" >Agregar Nueva</a>
    </div>
</div>
<br>
<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">Nombre</th>
        <th scope="col">Acción</th>

      </tr>
    </thead>
    <tbody>
        @foreach ($categories as $category)
        <tr>
            <th scope="row">{{$category->name}}</th>
            <td>
                <div>
                    <form action="{{ route('admin.categories.delete', $category)}}" method="post" style= "margin-bottom: 0px;">
                    {{ csrf_field() }}
                    @method('DELETE')
                    <button class="btn btn-danger" type="submit">Eliminar</button>
                    <a href="{{ route('admin.categories.edit', $category) }}" class="btn btn-secondary">Editar</a>
                </form>

                </div>

            </td>
        </tr>
        @endforeach

    </tbody>
  </table>


@endsection
