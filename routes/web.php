<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', function () {
    return view('shop');
});

Route::get('/product/{id}', function () {
    return view('shop');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin', function () {
    return view('admin/index');
});

Route::get('/admin/categories', 'CategoryController@index');
Route::get('/admin/categories/create', 'CategoryController@create')->name('admin.categories.create');
Route::get('/admin/categories/{id}', 'CategoryController@edit')->name('admin.categories.edit');
Route::post('/admin/categories', 'CategoryController@store')->name('admin.categories');
Route::put('/admin/categories/{id}', 'CategoryController@update')->name('admin.categories.update');
Route::delete('/admin/categories/{id}', 'CategoryController@destroy')->name('admin.categories.delete');

Route::get('/admin/marks', 'MarkController@index');
Route::get('/admin/marks/create', 'MarkController@create')->name('admin.marks.create');
Route::get('/admin/marks/{id}', 'MarkController@edit')->name('admin.marks.edit');
Route::post('/admin/marks', 'MarkController@store')->name('admin.marks');
Route::put('/admin/marks/{id}', 'MarkController@update')->name('admin.marks.update');
Route::delete('/admin/marks/{id}', 'MarkController@destroy')->name('admin.marks.delete');

Route::get('/admin/images', 'ImageController@index');
Route::get('/admin/images/create', 'ImageController@create')->name('admin.images.create');
Route::post('/admin/images', 'ImageController@store')->name('admin.images');
Route::delete('/admin/images/{id}', 'ImageController@destroy')->name('admin.images.delete');

Route::get('/admin/products', 'ProductController@index');
Route::get('/admin/products/create', 'ProductController@create')->name('admin.products.create');
Route::get('/admin/products/{id}', 'ProductController@edit')->name('admin.products.edit');
Route::get('/admin/products/{id}/show', 'ProductController@show')->name('admin.products.show');
Route::post('/admin/products', 'ProductController@store')->name('admin.products');
Route::put('/admin/products/{id}', 'ProductController@update')->name('admin.products.update');
Route::delete('/admin/products/{id}', 'ProductController@destroy')->name('admin.products.delete');

Route::get('/admin/sets', 'SetController@index');
Route::get('/admin/sets/create', 'SetController@create')->name('admin.sets.create');
Route::get('/admin/sets/{id}', 'SetController@edit')->name('admin.sets.edit');
Route::post('/admin/sets', 'SetController@store')->name('admin.sets');
Route::get('/admin/sets/{id}/show', 'SetController@show')->name('admin.sets.show');
Route::put('/admin/sets/{id}', 'SetController@update')->name('admin.sets.update');
Route::delete('/admin/sets/{id}', 'SetController@destroy')->name('admin.sets.delete');

Route::get('admin/products/deleteimage/{id}','ProductController@deleteimage')->name('admin.products.deleteimage');

Route::get('/pago/{id}', 'ShopController@pay')->name('init.pay');




